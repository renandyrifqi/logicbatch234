﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tugas1
{
    class Program
    {
        static void Main(string[] args)
        {
        menu:
            Console.WriteLine("List Soal");
            Console.WriteLine("+++++++++++++++++++++++++++++++++++++++++++++");
            Console.WriteLine("Soal 1");
            Console.WriteLine("Soal 2");
            Console.WriteLine("Soal 3");
            Console.WriteLine("Pilih Soal = ");
            string pilihan = Console.ReadLine();

            Console.WriteLine("=============================================");
            Console.WriteLine();
            switch (pilihan)
            {
                case "1":
                    Soal1();
                    break;
                case "2":
                    Soal2();
                    break;
                case "3":
                    Soal3();
                    break;
            }
            Console.WriteLine();
            Console.WriteLine("Pilih soal lagi ? Y/N");
            string lagi = Console.ReadLine();
            if (lagi.ToUpper() == "Y")
            {
                Console.Clear();
                goto menu;
            }
            Console.ReadKey();
        }
        public static void Soal1()
        {
            //Soal 1
            Console.WriteLine();
            Console.WriteLine("Soal 1");
            Console.Write("Input n deret: ");
            int n = int.Parse(Console.ReadLine());
            int j = 1;
            int a = 1;
            int b = 5;
            for (int i = 1; i <= n; i++)
            {
                if (j % 2 == 0)
                {
                    Console.Write(b + "\t");
                    b = b + 5;
                }
                else
                {
                    Console.Write(a + "\t");
                    a++;
                }
                j++;
            }

            Console.WriteLine();
        }


        public static void Soal2()
        {
            //Soal 2
            Console.WriteLine();
            Console.WriteLine("Soal 2");
            Console.Write("Input n deret: ");
            int n = int.Parse(Console.ReadLine());
            int j = 1;
            int a = 1;
            int b = 5;
            j = 1;
            a = 1;
            b = 5;
            for (int i = 1; i <= n; i++)
            {
                if (j % 3 == 0)
                {
                    Console.Write("*\t");
                }
                else if (j % 2 == 0)
                {
                    Console.Write(b + "\t");
                    b = b + 5;
                }
                else if (j % 5 == 0)
                {
                    Console.Write("@\t");
                }
                else
                {
                    Console.Write(a + "\t");
                    a++;
                }
                j++;
            }

            Console.WriteLine();
        }
        public static void Soal3()
        {
            Console.Write("Input n deret: ");
            int n = int.Parse(Console.ReadLine());
            int j = 1;
            int a = 1;
            int b = 5;
            //Soal 3
            Console.WriteLine();
            Console.WriteLine("Soal 3");
            j = 1;
            a = 1;
            b = 5;
            for (int i = 1; i <= n; i++)
            {
                if (n % 2 == 0)
                {
                    int t1 = n / 2;
                    int t2 = n / 2 + 1;
                    if (j == 1)
                    {
                        Console.Write("*\t");
                        a++;
                    }
                    else if (j == n)
                    {
                        Console.Write("*\t");
                    }
                    else if (j == t1 && j % 2 == 0)
                    {
                        Console.Write("@\t");
                        b = b + 5;
                    }
                    else if (j == t1 && j % 2 != 0)
                    {
                        Console.Write("@\t");
                        a++;
                    }
                    else if (j == t2 && j % 2 == 0)
                    {
                        Console.Write("@\t");
                        b = b + 5;
                    }
                    else if (j == t2 && j % 2 != 0)
                    {
                        Console.Write("@\t");
                        a++;
                    }
                    else if (j % 2 == 0)
                    {
                        Console.Write(b + "\t");
                        b = b + 5;
                    }
                    else if (j % 2 != 0)
                    {
                        Console.Write(a + "\t");
                        a++;
                    }
                }
                else
                {
                    int t = (n + 1) / 2;

                    if (j == 1)
                    {
                        Console.Write("*\t");
                        a++;
                    }
                    else if (j == n)
                    {
                        Console.Write("*\t");
                    }
                    else if (j == t && j % 2 == 0)
                    {
                        Console.Write("@\t");
                        b = b + 5;
                    }
                    else if (j == t && j % 2 != 0)
                    {
                        Console.Write("@\t");
                        a++;
                    }
                    else if (j % 2 == 0)
                    {
                        Console.Write(b + "\t");
                        b = b + 5;
                    }
                    else if (j % 2 != 0)
                    {
                        Console.Write(a + "\t");
                        a++;
                    }
                }
                j++;
            }
            Console.WriteLine();
            Console.ReadKey();
        }
    }
}