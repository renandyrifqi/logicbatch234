﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BootcampDay5
{
    class Program
    {
        static void Main(string[] args)
        {
            menu:
            Console.WriteLine("List Soal");
            Console.WriteLine("+++++++++++++++++++++++++++++++++++++++++++++");
            Console.WriteLine("Soal 1. Sortir Nilai");
            Console.WriteLine("Soal 2. Sopir Ojol");
            Console.WriteLine("Soal 3. Beli Pulsa Dapat Point");
            Console.WriteLine("Soal 4. Recursive Digit Sum");
            Console.WriteLine("Soal 5. I Am the one number");
            Console.Write("Pilih Soal = ");
            string pilihan = Console.ReadLine();
            Console.WriteLine("=============================================");
            Console.WriteLine();
            switch (pilihan)
            {
                case "1":
                    Nomor_1();
                    break;
                case "2":
                    Nomor_2();
                    break;
                case "3":
                    Nomor_3();
                    break;
                case "4":
                    Nomor_4();
                    break;
                case "5":
                    Nomor_5();
                    break;
            }
            Console.WriteLine();
            Console.WriteLine("Pilih soal lagi ? Y/N");
            string lagi = Console.ReadLine();
            if (lagi.ToUpper() == "Y")
            {
                Console.Clear();
                goto menu;
            }
            Console.ReadKey();
        }
        public static void Nomor_1()
        {
            Console.WriteLine("Soal 1 - Sorting Nilai");
            int terkecil, terbesar;
            Console.Write("Input deret nilai\t: ");
            string nilai = Console.ReadLine();
            int[] arr = nilai.Split(' ').Select(int.Parse).ToArray();
            for (int i = arr.Length - 1; i > 0; i--)
            {
                for (int j = 0; j < i; j++)
                {
                    if (arr[j] > arr[j + 1])
                    {
                        int temp = arr[j];
                        arr[j] = arr[j + 1];
                        arr[j + 1] = temp;
                    }
                }
            }
            Console.Write("Nilai Urut\t\t: ");
            for (int k = 0; k < arr.Length; k++)
            {
                Console.Write(arr[k] + " ");
            }
            terkecil = arr[0] + arr[1] + arr[2];
            terbesar = arr[arr.Length - 3] + arr[arr.Length - 2] + arr[arr.Length - 1];
            Console.WriteLine();
            Console.WriteLine("Jumlah 3 angka terkecil\t: " + terkecil);
            Console.WriteLine("Jumlah 3 angka terbesar\t: " + terbesar);
            Console.WriteLine();
        }
        public static void Nomor_2()
        {
            Console.WriteLine("Soal 2 - Sopir Ojol");
            Console.WriteLine("Keterangan Jarak Tempuh");
            Console.WriteLine("\t1) Jarak dari Toko ke Customer\t\t: 2 Km");
            Console.WriteLine("\t2) Jarak dari Customer 1 ke Customer 2\t: 500 m");
            Console.WriteLine("\t3) Jarak dari Customer 2 ke Customer 3\t: 1.5 Km");
            Console.WriteLine("\t4) Jarak dari Customer 3 ke Customer 4\t: 300 m");
            Console.WriteLine();
            int[] arah = new int[] { 2000, 500, 1500, 300 };
            Console.Write("Tujuan\t: ");
            string tujuan = Console.ReadLine();
            int[] jarak = tujuan.Split(' ').Select(int.Parse).ToArray();
            double total = 0;
            for (int i = 0; i < jarak.Length; i++)
            {
                total += arah[jarak[i] - 1];
            }
            Console.WriteLine();
            Console.WriteLine("Jarak yang ditempuh\t: " + total / 1000 + " m");
            Console.WriteLine("Bensin yang dibutuhkan\t: " + Math.Ceiling(total / 2500) + " liter");
            Console.WriteLine();
        }
        public static void Nomor_3()
        {
            Console.WriteLine("Soal 3 - Transaksi Pulsa Dapat Point");
            Console.WriteLine("Bonus Poin Pembelian Pulsa;");
            Console.WriteLine("\t1) 0 - 10.000\t\t: 2 km");
            Console.WriteLine("\t2) 10.001 - 30.000\t: 500 m");
            Console.WriteLine("\t3) >30.000\t\t: 1.5 km");
            Console.WriteLine();
            Console.Write("Pembelian pulsa\t: ");
            int beli = int.Parse(Console.ReadLine());
            int[] poin = new int[3] { 0, 1, 2 };
            int tempPoin = 0;
            int totPoin = 0;
            for (int i = poin.Length - 1; i < poin.Length && i >= 0; i--)
            {
                if (beli > 30000)
                {
                    tempPoin = beli;
                    tempPoin -= 30000;
                    tempPoin = (tempPoin / 1000) * poin[i];
                    beli -= (beli - 30000);
                    totPoin += tempPoin;
                }
                else if (beli > 10000 && beli <= 30000)
                {
                    i = 1;
                    tempPoin = beli;
                    tempPoin -= 10000;
                    tempPoin = (tempPoin / 1000) * poin[i];
                    beli -= (beli - 10000);
                    totPoin += tempPoin;
                }
            }
            Console.WriteLine("Total bonus poin yang didapat " + totPoin);
        }
        public static void Nomor_4()
        {
            Console.WriteLine("Soal 4 - Recursive Digit Sum");
            Console.WriteLine("Soal No.4 - Recursive");
            Console.Write("Input Recursive\t: ");
            string aNilai = Console.ReadLine();
            int[] a = aNilai.Split(' ').Select(int.Parse).ToArray();
            char[] pecahan = a[0].ToString().ToCharArray();

            int temp = 0;
            int data = 0;

            rekursif:
            for (int j = 0; j < pecahan.Length; j++)
            {
                data = int.Parse(pecahan[j].ToString());
                temp += data;
                Console.WriteLine(temp);
            }
            temp *= a[1];
            if (temp > 9)
            {
                pecahan = temp.ToString().ToCharArray();
                temp = 0;
                a[1] = 1;
                goto rekursif;
            }
            Console.WriteLine("hasil\t: " + temp);
        }
        public static void Nomor_5()
        {
            Console.WriteLine("Soal No 5 - I am The One Number");
            Console.WriteLine("Masukkan Jumlah Angka= ");
            int input = int.Parse(Console.ReadLine());
            int count = 0;

            for (int i = 100; i < 1000; i++)
            {
                string angka = i.ToString();
                repeat:
                int[] square = new int[angka.Length];
                for (int j = 0; j < square.Length; j++)
                {
                    string temp = "";
                    temp += angka[j];
                    square[j] = int.Parse(temp) * int.Parse(temp);
                }
                int sum = 0;
                for (int k = 0; k < square.Length; k++)
                {
                    sum += square[k];
                }
                if (sum == 1)
                {
                    Console.WriteLine(i + " Is the one number");
                    count++;
                }
                else if (sum > 9)
                {
                    angka = sum.ToString();
                    goto repeat;
                }
                if (count == input)
                {
                    break;
                }
            }
            Console.ReadLine();
        }

    }
}