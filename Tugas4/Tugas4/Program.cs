﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tugas4
{
    class Program
    {
        static void Main(string[] args)
        {
            menu:
            Console.WriteLine("List Soal");
            Console.WriteLine("+++++++++++++++++++++++++++++++++++++++++++++");
            Console.WriteLine("Soal 1. Sortir Vokal dan Konsonan");
            Console.WriteLine("Soal 2. Time Conversion");
            Console.WriteLine("Pilih Soal = ");
            string pilihan = Console.ReadLine();
            Console.WriteLine("=============================================");
            Console.WriteLine();
            switch (pilihan)
            {
                case "1":
                    Soal1();
                    break;
                case "2":
                    Soal2();
                    break;

            }
            Console.WriteLine();
            Console.WriteLine("Pilih soal lagi ? Y/N");
            string lagi = Console.ReadLine();
            if (lagi.ToUpper() == "Y")
            {
                goto menu;
            }
            Console.ReadKey();
        }
        public static void Soal1()
        {
            Console.WriteLine("Soal 1. Sortir Vokal dan Konsonan");
            Console.Write("Masukan Kalimat = ");
            string kalimat = Console.ReadLine();
            string[] array = kalimat.ToUpper().Split(' ');
            string[] vokal = { "A", "I", "U", "E", "O" };
            string vkl = "";
            string k = "";
            for (int i = 0; i < array.Length; i++)
            {
                char[] kata = array[i].ToCharArray();
                for (int a = 0; a < kata.Length; a++)
                {
                    int index1 = Array.IndexOf(vokal, kata[a].ToString().ToUpper());
                    if (index1 != -1)
                    {
                        vkl += kata[a].ToString();
                    }
                    else
                    {
                        k += kata[a].ToString();
                    }
                }
            }
            //pengurutan
            char[] vklchar = vkl.ToCharArray();
            char[] kchar = k.ToCharArray();
            for (int x = 0; x < vkl.Length; x++)
            {
                for (int z = 0; z < vkl.Length - 1; z++)
                {
                    char temp = ' ';
                    if (vklchar[z] > vklchar[z + 1])
                    {
                        temp = vklchar[z];
                        vklchar[z] = vklchar[z + 1];
                        vklchar[z + 1] = temp;
                    }
                }
            }
            Console.WriteLine(string.Join(" ", vklchar));
            Array.Sort(kchar);
            Console.WriteLine(string.Join(" ", kchar));

        }
        public static void Soal2()
        {
            Console.WriteLine("Soal 2. Time Conversion");
            Console.Write("Masukan Input = ");
            string input = Console.ReadLine();
            string hasil = " ";
            int hitung = 0;
            for (int i = 0; i < input.Length; i++)
            {
                hasil = input.Substring(i, 1);
                if (hasil == "P")
                {
                    hitung = 1;
                }
            }
            if (hitung != 1)
            {
                string[] array = input.Split('A');
                Console.WriteLine("Hasilnya adalah =  " + array[0]);
            }
            else
            {
                string[] array = input.Split('P');
                string[] array1 = array[0].Split(':');
                int bilpertama = Convert.ToInt32(array1[0]);
                if (bilpertama != 12)
                {
                    bilpertama = bilpertama + 12;
                }
                Console.WriteLine("Hasilnya adalah =" + bilpertama + ":" + array1[1] + ":" + array1[2]);
            }
        }
    }
}
