﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BootcampDay2
{
    class Program
    {
        static void Main(string[] args)
        {
            menu:
            Console.WriteLine("List Soal");
            Console.WriteLine("+++++++++++++++++++++++++++++++++++++++++++++");
            Console.WriteLine("Soal 1. Fibonacci 2");
            Console.WriteLine("Soal 2. Fibonacci 3");
            Console.WriteLine("Soal 3. Bilangan Prima");
            Console.WriteLine("Soal 4. Pohon Faktorial");
            Console.WriteLine("Soal 5. Pohon Faktorial");
            Console.WriteLine("Pilih Soal = ");
            string pilihan = Console.ReadLine();
            Console.WriteLine("=============================================");
            Console.WriteLine();
            switch (pilihan)
            {
                case "1":
                    Soal1();
                    break;
                case "2":
                    Soal2();
                    break;
                case "3":
                    Soal3();
                    break;
                case "4":
                    Soal4();
                    break;
                case "5":
                    Soal5();
                    break;
            }
            Console.WriteLine();
            Console.WriteLine("Pilih soal lagi ? Y/N");
            string lagi = Console.ReadLine();
            if (lagi.ToUpper() == "Y")
            {
                Console.Clear();
                goto menu;
            }
            Console.ReadKey();
        }
        public static void Soal1()
        {
            //soal 1
            Console.Write("Input n deret: ");
            int n = int.Parse(Console.ReadLine());
            Console.WriteLine("------------------------------");
            Console.WriteLine(" ");
            Console.WriteLine("Soal 1  #Fibonacci 2");
            int a = 0;
            int b = 1;
            int f = 1;
            for (int i = 1; i <= n; i++)
            {
                Console.Write(f + " ");
                f = a + b;
                a = b;
                b = f;
            }
            Console.WriteLine("");
            Console.WriteLine("=================================");
        }
        public static void Soal2()
        {
            //soal 2
            Console.WriteLine("Soal 2  #Fibonacci 3");
            Console.WriteLine();
            Console.Write("Input n deret: ");
            int n = int.Parse(Console.ReadLine());
            int a = 0;
            int b = 1;
            int f = 1;

            a = 1;
            b = 1;
            int c = 1;
            f = 1;
            Console.Write(a + " " + b + " " + c + " ");
            for (int i = 3; i < n; i++)
            {
                f = a + b + c;
                a = b;
                b = c;
                c = f;
                Console.Write(f + " ");
            }

            Console.WriteLine(" ");
            Console.WriteLine("=================================");
        }
        public static void Soal3()
        {
            ////soal 3
            Console.WriteLine("Soal 3  #Bilangan Prima");
            Console.WriteLine();
            Console.Write("Input n deret: ");
            int n = int.Parse(Console.ReadLine());
            int a = 0;
            int b = 1;
            a = 0;
            b = 0;
            for (int i = 2; i <= n; i++)
            {
                a = 2;
                for (a = a; a <= i - 1; )
                {
                    if (i % a == 0)
                    {
                        b = b + 1;
                    }
                    a++;
                }

                if (b == 0)
                {
                    Console.Write(i + " ");
                }
                b = 0;
            }
            Console.WriteLine(" ");
            Console.WriteLine("=================================");
        }
        public static void Soal4()
        {
            //soal 4
            Console.WriteLine("Soal 4  #Pohon Faktorial");
            Console.Write("Input Nilai : ");
            int angka = int.Parse(Console.ReadLine());
            int hasil = angka;
            int j = 0;
            for (int i = 0; i <= angka; i++)
            {
                for (j = 2; j <= i; j++)
                {
                    if (angka % j == 0)
                    {
                        Console.Write(j + " ");
                        angka /= j;
                    }
                    if (j == angka)
                    {
                        Console.Write(j + " ");
                        angka /= j;
                    }
                }
            }

            if (angka == 1 || angka == hasil)
            {
                Console.Write("1");
            }
            Console.WriteLine(" ");
            Console.WriteLine("=================================");
        }
        public static void Soal5()
        {
            //Soal 5
            Console.WriteLine("Soal 5  #Pohon Faktorial");
            Console.Write("Input Nilai : ");
            int angka = int.Parse(Console.ReadLine());
            int hasil = angka;
            int j = 0;
            hasil = angka;
            int pembagi = 0;
            j = 0;
            for (int i = 0; i <= angka; i++)
            {
                for (j = 2; j <= i; j++)
                {
                    if (angka % j == 0)
                    {
                        Console.Write(j + " ");
                        pembagi += j;
                        angka /= j;
                    }
                    if (j == angka)
                    {
                        Console.Write(j + " ");
                        pembagi += j;
                        angka /= j;
                    }
                }
            }

            if (angka == 1 || angka == hasil)
            {
                Console.Write("1");
                pembagi += 1;
            }
            Console.WriteLine();
            Console.WriteLine("Jumlah nilai pembagi = " + pembagi);
            Console.WriteLine("");
            Console.ReadKey();
        }
    }
}