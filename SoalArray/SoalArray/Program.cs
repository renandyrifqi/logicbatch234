﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoalArray
{
    class Program
    {
        static void Main(string[] args)
        {
            menu:
            Console.WriteLine("List Soal");
            Console.WriteLine("+++++++++++++++++++++++++++++++++++++++++++++");
            Console.WriteLine("Soal 1. SOS");
            Console.WriteLine("Soal 2. Array Rotation");
            Console.WriteLine("Soal 3. Puntung Rokok");
            Console.WriteLine("Soal 4. Belanja Baju dan Celana");
            Console.WriteLine("Soal 5. Palindrome");
            Console.WriteLine("Pilih Soal = ");
            string pilihan = Console.ReadLine();
            Console.WriteLine("=============================================");
            Console.WriteLine();
            switch (pilihan)
            {
                case "1":
                    Nomor_1();
                    break;
                case "2":
                    Nomor_2();
                    break;
                case "3":
                    Nomor_3();
                    break;
                case "4":
                    Nomor_4();
                    break;
                case "5":
                    Nomor_5();
                    break;
            }
            Console.WriteLine();
            Console.WriteLine("Pilih soal lagi ? Y/N");
            string lagi = Console.ReadLine();
            if (lagi.ToUpper() == "Y")
            {
                Console.Clear();
                goto menu;
            }
            Console.ReadKey();
        }
        public static void Nomor_1()
        {
            Console.WriteLine("Tugas Array");
            Console.WriteLine("=============================================");
            Console.WriteLine(" ");

            // Soal No.1
            Console.Write("Masukkan kode (kelipatan 3) : ");
            string input = Console.ReadLine();
            int count = 0;
            if (input.Length % 3 != 0 || input.Length == 0)
            {
                Console.WriteLine("Input harus kelipatan 3. Mohon restart program dan coba lagi.");
            }
            else
            {
                for (int i = 0; i < input.Length; i += 3)
                {
                    if (input.Substring(i, 3).ToLower() == "sos")
                    {
                        count++;
                    }
                }
                Console.WriteLine("Jumlah kode SOS valid = " + count);
                Console.WriteLine("Jumlah kode SOS yang tidak valid = " + (input.Length / 3 - count));

            }
        }

        public static void Nomor_2()
        {
            Console.WriteLine("================================================");
            Console.WriteLine(" ");
            //Soal No.2
            Console.WriteLine("Soal Rotation");
            string[] arr = new string[5] { "5", "6", "7", "0", "1" };
            string output = string.Join(",", arr);
            string temp0 = "";
            string temp1 = "";

            Console.WriteLine("Original Array = " + output);
            Console.Write("Masukan Banyaknya rotasi = ");
            int nrot = int.Parse(Console.ReadLine());

            for (int i = 0; i < nrot; i++)
            {
                for (int j = arr.Length - 1; j >= 0; j--)
                {
                    if (j == arr.Length - 1)
                    {
                        temp0 = arr[j];
                        arr[j] = arr[0];
                    }
                    else
                    {
                        temp1 = arr[j];
                        arr[j] = temp0;
                        temp0 = temp1;
                    }
                }
                Console.WriteLine("rotasi ke " + i + " = " + string.Join(",", arr));
            }
        }
        public static void Nomor_3()
        {
            Console.WriteLine("================================================");
            Console.WriteLine(" ");
            //Soal No. 3
            Console.WriteLine("Soal Rokok");
            Console.Write("Masukan n = ");
            int n = int.Parse(Console.ReadLine());
            int jmlPuntung = n / 8;
            int hasil = jmlPuntung * 500;
            Console.WriteLine("Jumlah Puntung yang dikumpulkan = " + jmlPuntung + " batang");
            Console.WriteLine("Penghasilan yang didapat = RP." + hasil);
        }
        public static void Nomor_4()
        {
            Console.WriteLine("================================================");
            Console.WriteLine(" ");
            //Soal No. 4
            Console.WriteLine("Soal Belanja Lebaran");
            Console.WriteLine("");
            Console.WriteLine("Uang Andi= ");
            int uang = int.Parse(Console.ReadLine());
            Console.Write("Masukkan jumlah pasangan baju dan celana : ");
            int jumlah = int.Parse(Console.ReadLine());

            int[] baju = new int[jumlah];
            int[] celana = new int[jumlah];
            int[] gabungan = new int[jumlah];
            int best = 0;

            for (int i = 0; i < jumlah; i++)
            {
                Console.Write("Masukkan harga baju " + (i + 1) + " : ");
                int input = int.Parse(Console.ReadLine());
                baju[i] = input;
                Console.Write("Masukkan harga celana " + (i + 1) + " : ");
                input = int.Parse(Console.ReadLine());
                celana[i] = input;
            }
            for (int i = 0; i < jumlah; i++)
            {
                gabungan[i] = baju[i] + celana[i];
                if (gabungan[i] <= uang)
                {
                    if (gabungan[i] > gabungan[best])
                    {
                        best = i;
                    }
                }
            }
            //Console.WriteLine (string.Join(",", baju));
            if (gabungan[best] > uang)
            {

                Console.WriteLine("Maaf, kombinasi pakaian diluar budget anda");
            }
            else
            {
                Console.WriteLine("Pilihan terbaik = " + gabungan[best]);
                Console.WriteLine("Silahkan ambil paket " + (best + 1));
            }
        }
        public static void Nomor_5()
        {
            Console.WriteLine("================================================");
            Console.WriteLine(" ");
            //Soal No. 5
            Console.WriteLine("Soal Belanja Lebaran");
            Console.WriteLine("");
            string temp = "";
            Console.Write("Masukkan kata = ");
            string kata = Console.ReadLine();
            Console.Write("\t");
            string[] split = kata.Split(' ');

            for (int j = 0; j < split.Length; j++)
            {
                string tempkata = split[j];
                for (int i = tempkata.Length - 1; i >= 0; i--)
                {
                    temp += tempkata[i].ToString();
                }
                Console.Write("\t");
                if (temp.ToUpper() == split[j].ToUpper())
                {
                    Console.Write("Yes ");
                    temp = "";
                }
                else
                {
                    Console.Write("No ");
                    temp = "";
                }
            }
        }
    }
}